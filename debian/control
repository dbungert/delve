Source: delve
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Emanuel Krivoy <ekrivoy@gmail.com>,
           Shengjing Zhu <zhsj@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-cosiner-argv-dev,
               golang-github-creack-pty-dev,
               golang-github-google-go-dap-dev (>= 0.4.0~),
               golang-github-hashicorp-golang-lru-dev,
               golang-github-mattn-go-isatty-dev,
               golang-github-peterh-liner-dev,
               golang-github-pkg-profile-dev,
               golang-github-sirupsen-logrus-dev,
               golang-github-spf13-cobra-dev (>= 1.1.1~),
               golang-golang-x-arch-dev,
               golang-golang-x-sys-dev,
               golang-gopkg-yaml.v2-dev,
               golang-starlark-dev,
               lsof <!nocheck>,
Standards-Version: 4.5.1
Homepage: https://github.com/go-delve/delve
Vcs-Browser: https://salsa.debian.org/go-team/packages/delve
Vcs-Git: https://salsa.debian.org/go-team/packages/delve.git
XS-Go-Import-Path: github.com/go-delve/delve
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-go

Package: delve
Architecture: amd64 arm64 i386
Built-Using: ${misc:Built-Using},
Depends: ${misc:Depends},
         ${shlibs:Depends},
Description: debugger for the Go programming language
 Delve enables you to interact with your program by controlling the execution
 of the process, evaluating variables, and providing information of
 thread/goroutine state, CPU register state and more.
